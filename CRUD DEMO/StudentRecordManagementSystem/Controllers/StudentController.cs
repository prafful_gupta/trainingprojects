﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StudentRecordManagementSystem.Models;

namespace StudentRecordManagementSystem.Controllers
{
    public class StudentController : Controller
    {
        StudentDAL studentDAL = new StudentDAL();
        public IActionResult Index()
        {
            List<Student> stud = new List<Student>();
            stud = studentDAL.GetStudents().ToList();
            return View(stud);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create([Bind] Student student)
        {
            if (ModelState.IsValid)
            {
                studentDAL.AddStudent(student);
                return RedirectToAction("Index");
            }
            return View(student);
        }



        public IActionResult Edit(int id)
        {
            Student student = studentDAL.GetStudentById(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]

        public IActionResult Edit(int id, [Bind] Student student)
        {
            if (ModelState.IsValid)
            {
                studentDAL.UpdateStudent(student);
                return RedirectToAction("Index");
            }
            return View(studentDAL);
        }

        [HttpGet]
        public IActionResult Details(int id)
        {
            Student student = studentDAL.GetStudentById(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        public IActionResult Delete(int id)
        {
            Student student = studentDAL.GetStudentById(id);
            if (student == null)
            {
                return NotFound();
            }
            return View(student);
        }

        [HttpPost , ActionName("Delete")]
        [ValidateAntiForgeryToken]

        public IActionResult DeleteStudent(int id)
        {
            studentDAL.DeleteStudent(id);
            return RedirectToAction("Index");
        }
    }
}