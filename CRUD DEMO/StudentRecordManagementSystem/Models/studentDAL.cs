﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;

using System.Data.SqlClient;
using System.Configuration;

namespace StudentRecordManagementSystem.Models
{
    public class StudentDAL
    {
        string connectionString = "Data Source=DESKTOP-1C9S6B8;Initial Catalog=StudentRecordDB;Integrated Security=False; Persist Security Info = False; User ID = sa; password = hrhk;";
        //To get all records.
        public IEnumerable<Student> GetStudents()
        {
            List<Student> stuList = new List<Student>();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_GetStudents", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Student student = new Student();
                    student.Id = Convert.ToInt32(dr["Id"].ToString());
                    student.Name = dr["Name"].ToString();
                    student.Gender = dr["Gender"].ToString();
                    student.Age = Convert.ToInt32(dr["Age"].ToString());
                    student.Percentage = Convert.ToDouble(dr["Percentage"].ToString());
                    stuList.Add(student);

                }
                con.Close();
            }
            return stuList;
        }

        //To insert a student record.
        public void AddStudent(Student student)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_InsertStudent", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@name", student.Name);
                cmd.Parameters.AddWithValue("@gender", student.Gender);
                cmd.Parameters.AddWithValue("@age", student.Age);
                cmd.Parameters.AddWithValue("@percentage", student.Percentage);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        //To Update Student Record.

        public void UpdateStudent(Student student)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_UpdateStudent", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", student.Id);
                cmd.Parameters.AddWithValue("@name", student.Name);
                cmd.Parameters.AddWithValue("@gender", student.Gender);
                cmd.Parameters.AddWithValue("@age", student.Age);
                cmd.Parameters.AddWithValue("@percentage", student.Percentage);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        //To Delete Student Record.

        public void DeleteStudent(int id)
        {
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_DeleteStudent", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
        }

        //Get Student Record By Id.

        public Student GetStudentById(int id)
        {
            Student student = new Student();

            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("SP_GetStudentById", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@id", id);
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    student.Id = Convert.ToInt32(dr["Id"].ToString());
                    student.Name = dr["Name"].ToString();
                    student.Gender = dr["Gender"].ToString();
                    student.Age = Convert.ToInt32(dr["Age"].ToString());
                    student.Percentage = Convert.ToDouble(dr["Percentage"].ToString());

                }
                con.Close();
            }
            return student;
        }
    }
}
